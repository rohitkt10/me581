#!/usr/bin/env python

"""

AUTHOR : Rohit Tripathy

DATE :  11/ 3/ 2014


The following code is a general subroutine to map any general
quadrilateral region into a standard [-1, 1] x [-1, 1] region.

The code does the following:

1. Defines a function to take in the coordinates of the vertices
of the general quadrilateral and computes the transformation
functions by making use of shape functions.

2. Defines a function that computes the jacobian of U(u, v) and
V(u, v).

3. Defines a function that computes the integral of a function
in the standard [-1, 1]x[-1, 1] space in the (u, v) coordinate
system.

"""

#import relevant packages
import numpy as np
from sympy import *
import math

#function to compute the Jacobian of two functions of u & v.
def jacobian(f, g):
    """
    The input functions for computing the Jacobian have
    to be in symbolic form
    """

    j11 = diff(f, u)
    j12 = diff(f, v)
    j21 = diff(g, u)
    j22 = diff(g, v)
    jdet = (j11 * j22) - (j12 * j21)
    return jdet


#Define the transformation
def transformation(x_list, y_list):
    """
    This routine define the transformation of a general quadrilateral space in
    Cartesian Coordinates and transforms it to a [-1, 1]x[-1, 1] space in a
    (u, v) coordinate system
    
    Parameters:
    x_list :: type = list
    y_list :: type = list
    
    Returns:
    U :: type - symbolic function
    V :: type - symbolic function
    """
    
    x1 = x_list[0]
    x2 = x_list[1]
    x3 = x_list[2]
    x4 = x_list[3]
    y1 = y_list[0]
    y2 = y_list[1]
    y3 = y_list[2]
    y4 = y_list[3]
    
    #define shape functions
    N1 = ((1 - u) * (1 - v)) / 4
    N2 = ((1 + u) * (1 - v)) / 4
    N3 = ((1 + u) * (1 + v)) / 4
    N4 = ((1 - u) * (1 + v)) / 4
    
    #Define U(u, v) and V(u, v)
    U = (N1 * x1) + (N2 * x2) + (N3 * x3) + (N4 * x4)
    V = (N1 * y1) + (N2 * y2) + (N3 * y3) + (N4 * y4)
    
    return U, V
 

#Define the two point Gaussian Quadrature algorithm
def two_point_gauss(f):
    """
    A general subroutine to compute double
    integral of a function over the standard
    space [-1, 1]x[-1, 1]
    
    Parameters:
      f :: type - symbolic function
    
    Returns:
      The evaluated integral over the standard space.
    """
    p = 1 / math.sqrt(3.)
    I_inner = F.subs(u, p) + F.subs(u, -p)
    I = I_inner.subs(v, p) + I_inner.subs(v, p)
    return I
    
if __name__ == '__main__':
    
    #Output file
    fl = open("Homework 5 Output.txt", 'w+')
    
    #define symbols
    u = Symbol('u')
    v = Symbol('v')
    x = Symbol('x')
    y = Symbol('y')
    
    #Problem 5
    fl.write("Problem 5\n================\n")
    
    #The general quadrilateral region from problem 5 has to be mapped
    fl.write(" The general region in Problem 5 is mapped onto the standard region using ")
    fl.write("the transformation functions U(u, v) and V(u, v) given by:: \n")
    
    x_list = [0., 2., 4., 0.]
    y_list = [0., 0., 4., 4.]
    U, V = transformation(x_list, y_list)
    fl.write("U(u, v) = " + str(U) )
    fl.write("\nV(u, v) = " + str(V) + "\n\n")
    
    #Compute Jacobian:
    J = jacobian(U, V)
    
    #Print the Jacobian
    fl.write("The Jacobian is given by ::\n")
    fl.write("\nJ(U, V) = " + str(J))
    
    #Problem 7
    fl.write("\n\nProblem 7\n================\n")
    #Define the transformation functions U(u, v) and V(u, v)
    x_list = [-3., -1., 3., -1.]
    y_list = [-2., -2., 2., 2.]
    U, V = transformation(x_list, y_list)
    
    #Find Jacobian of U(u, v) and V(u, v)
    J = jacobian(U, V)
    
    fl.write("\nPart (a) \n---------\n\n" )
    
    #Part(a) :: f(x, y) = xy. Define f
    f = x * y

    #Find the integrand F(u, v)
    f = f.subs(x, U)
    f = f.subs(y, V)
    F = f * J
    
    #Use two point Gaussian quadrature to evaluate the integral
    #First compute the inner integral
    p = 1 / math.sqrt(3.)
    I_inner = F.subs(u, p) + F.subs(u, -p)
    
    #Now, compute the outer integral
    I = two_point_gauss(F)
    
    #Print result
    fl.write("The evaluated integral is: ")
    fl.write(repr(I))
    
    fl.write("\nPart (b) \n---------\n\n" )
    #Part(b) :: f(x, y) = xy*exp(-x). Define f
    f = x * y * (exp(-x))

    #Find the integrand F(u, v)
    f = f.subs(x, U)
    f = f.subs(y, V)
    
    F = f * J
    
    #Use two point Gaussian quadrature to evaluate the integral
    #First compute the inner integral
    p = 1 / math.sqrt(3.)
    I_inner = F.subs(u, p) + F.subs(u, -p)
    
    #Now, compute the outer integral
    I = two_point_gauss(F)
    
    #Print result
    fl.write("The evaluated integral is: ")
    fl.write(repr(I))
    exit()



