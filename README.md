# README #

This repository consists of all the codes written for the homework of the course ME581. 

* HW 1: 
Codes for : 

 a. Newton Raphson Method

 b. Bisection Method

* HW 2:
Codes for :

 a. Gauss Elimination using partial pivoting

 b. Gauss Elimination using scaled pivoting

* HW 3:
Codes for :

 a. Gauss Seidel Approximation

