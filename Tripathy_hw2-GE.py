"""
   AUTHOR : ROHIT TRIPATHY 
   
   DATE : 09/19/2014 
   
   The following is a code to compute the solution of a given system of 
   linear equations using partial and scaled pivoting. 
   
   The augmented matrix is given and passed as argument to the functions
   corresponding to the functions for partial and scaled pivoting. 
   
"""

#import required library packages
import numpy as np 
from math import fabs

def scaled_pivoting(a1):
	"""
	This is a function to implement the scaled pivoting technique to 
	compute the solution of a system of linear equations.
	
	PRECONDITION : The coefficient matrix should be non singular.
	"""
	n=len(a1)
	s=np.ones(n)
	l = 0
	#Find the largest element in each row
	for i in range(n):
		s[i] = a1[i][0]	
		for j in range(n):
			if(a1[i][j] > s[i]):
				s[i] = a1[i][j]
	for p in range(0,n):
		pivot = a1[p][p]
		
	    #Determine the location of the pivot
		for k in range(p+1, n):
			if(fabs(a1[k][p] / s[k]) > fabs(pivot / s[p])):
				l = k
				pivot = a1[k][p]

	    #Exchange rows if pivot location has changed 
		if(l != p):
			for t in range(p, n+1):
				temp = a1[l][t]
				a1[l][t] = a1[p][t]
				a1[p][t] = temp
				
		#Make other elements in the column zero		
		for k in range(p+1, n):
			m = -a1[k][p] / a1[p][p]
			for j in range(p, n+1):
				if j == p:
					a1[k][j] = 0
				else:
					a1[k][j] += m * a1[p][j]
					
	#Compute the x column vector
	x=[0 for i in range(n)]	
	for i in range(n-1, -1, -1):
		x[i] = a1[i][n] / a1[i][i]
		for j in range(i-1, -1, -1):
			a1[j][n] -= a1[j][i] * x[i]
			
	#Return the x column vector
	return x
	
		
def partial_pivoting(a):
	"""
	This is a function to implement the partial pivoting technique to 
	compute the solution of a system of linear equations.
	
	PRECONDITION : The coefficient matrix should be non-singular.
	"""
	l=0
	n = len(a)
	for p in range(0, n):
		pivot=a[p][p]    #Set initial pivot
		
		# Find the largest element and its location in the current
		#column
		for k in range(p+1, n):
			if(fabs(a[k][p]) > fabs(pivot)):
				l = k
				pivot=a[k][p]
				
		#Exchange rows if pivot has changed
		if(l != p):
			for t in range(p, n+1):
				temp = a[l][t]
				a[l][t] = a[p][t]
				a[p][t] = temp
				
		#Make other elements in the column zero		
		for k in range(p+1, n):
			m = -a[k][p]/a[p][p]
			for j in range(p, n+1):
				if p == j:
					a[k][j] = 0
				else:
					a[k][j] += m * a[p][j]
					
	#Compute the x column vector
	x=[0 for i in range(n)]	
	for i in range(n-1, -1, -1):
		x[i] = a[i][n] / a[i][i]
		for j in range(i-1, -1, -1):
			a[j][n] -= a[j][i] * x[i]
					
	#Return the x column vector
	return x
	
f = open('Pivoting.txt', 'w+')

#Example Problem
a1 = ([3., 1., 4., -1., 7.], [2., -2., -1., 2., 1.], [5., 7., 14., -8., 20.], [1., 3., 2., 4., -4.])
a2 = ([3., 1., 4., -1., 7.], [2., -2., -1., 2., 1.], [5., 7., 14., -8., 20.], [1., 3., 2., 4., -4.])
n=len(a1)
x = scaled_pivoting(a1)
print x
x = partial_pivoting(a2)
print x


#Problem 1(a) on page 168	
a1 = np.loadtxt('input1.txt')
a2 = np.loadtxt('input1.txt')
f.write("Problem 1(a) on page 168:\nThe initial augmented matrix is : \n\n")
n=len(a1)
for i in range(n):
	for j in range(n+1):
		f.write(repr(a1[i][j])+"    ")
	f.write("\n")
	
x1=scaled_pivoting(a1)
f.write("\nThe elements of the solution vector correspond to the values of x1, x2, x3 and x4\n")
f.write("\nWith scaled pivoting, the solution vector is :\n")
for i in range(n):
	f.write(repr(x1[i])+"  ")

f.write("\n\nWith partial pivoting, the solution vector is :\n")
x2=partial_pivoting(a2)
for i in range(n):
	f.write(repr(x2[i])+"  ") 


#Problem 1(e) on page 168
f.write("\n\n\nProblem 1(e) on page 168:\nThe initial augmented matrix is : \n\n")
a1 = np.loadtxt('input2.txt')
a2 = np.loadtxt('input2.txt')
n=len(a1)
for i in range(n):
	for j in range(n+1):
		f.write(repr(a1[i][j])+"    ")
	f.write("\n")
x1=scaled_pivoting(a1)
f.write("\nThe elements of the solution vector correspond to the values of x1, x2, x3 and x4\n")
f.write("\nWith scaled pivoting, the solution vector is :\n")
for i in range(n):
	f.write(repr(x1[i])+"  ")
f.write("\n\nWith partial pivoting, the solution vector is :\n")
x2=partial_pivoting(a2)
for i in range(n):
	f.write(repr(x2[i])+"  ")


#Problem 15 on page 190
f.write("\n\n\nProblem 15 on page 190:\nThe initial augmented matrix is : \n\n")
a1 = np.loadtxt('input3.txt')
n = len(a1)
for i in range(n):
	for j in range(n+1):
		f.write(str(a1[i][j])+"    ")
	f.write("\n")
x=scaled_pivoting(a1)
f.write("\nThe elements of the solution vector correspond to the values of x1, x2, x3 and x4\n")
f.write("\nWith scaled pivoting, the solution vector is :\n")
for i in range(n):
	f.write(str(x[i])+"  ")


#Problem 17 on page 204
f.write("\n\n\nProblem 15 on page 190:\nThe initial augmented matrix is : \n\n")
a1 = np.loadtxt('input4.txt')
n = len(a1)
for i in range(n):
	for j in range(n+1):
		f.write(str(a1[i][j])+"    ")
	f.write("\n")
x=scaled_pivoting(a1)
f.write("\nThe elements of the solution vector correspond to the values of F1 through F9 and then Fh, Fv and Fr\n")
f.write("\nWith scaled pivoting, the solution vector is :\n")
for i in range(n):
	f.write(repr(x[i])+"  ")
