#!/usr/bin/env python

"""
    The following is a code to compute the solution to a system of
    linear equations expressed in the form AX = B using the
    Gauss-Seidel Method.
    
    AUTHOR : Rohit Tripathy
    
    DATE : 10/2/2014
    
"""

#Importing relevant packages
import numpy as np
import matplotlib.pyplot as plt
import math as m

def check_diag_dominant(x, n):
    """
    This function takes in two inputs - a square matrix x and the
    dimensionality of the square matrix.
    
    It returns true if the matrix is diagonally dominant. Else it
    returns false
    """
    flag = 1.0
    sum = 0.0
    for i in range(n):
        sum = 0.0
        for j in range(n):
            if(i != j):
                sum += m.fabs(x[i][j])
        if(m.fabs(x[i][i]) < sum):
            flag = 0.0
            break
    if(flag == 1.0):
        return True
    else:
        return False


def check_positive_definite(x, n):

    """
    This function is used to determine weather the matrix square
    matrix x with dimensionality n is postive definite. A matrix A
    is said to positive definite if all its Eigen Values are postive.
    
    The current function will return true if all the eigen values are
    postive i.e. the matrix is positive definite. Else the function will
    return false.
    """
    return np.all(np.linalg.eigvals(x) > 0)
    
    
def gauss_seidel(a, b, n):
    """
        Defining the function to compute the solution to the system
        of equations.
        
        PRE-CONDITIONS:
        The convergence of the Gauss Seidel approach Seidel relies on
        the coefficient matrix A satisfying the following conditions:
        
        1. The matrix A is positive definite
        2. The matrix A is diagonally dominant
        
        The initial approximation for all elements is taken to be zero
        
        When the value of |x(n+1) -x(n)| < 1e-6, the convergence is established
        and the algorithm is terminated and the resulting solution vector is
        returned by the function
    """
    
    #Check for diagonal dominance and positive definite-ness of the
    #coefficient matrix
    assert check_positive_definite(a, n) == True
    assert check_diag_dominant(a, n) == True
    
    #Define a flag variable
    flag = 0.0
    sum =0.0
    #Define two matrix to compute solution, compare for error after each
    #successive iterations and initialize both as row vectors with all
    #elements as zero.
    x1 = np.zeros(n)
    x2 = np.zeros(n)
    e = np.ones(n)
    count = 0
    
    #Begin a while loop to perform iterations until convergence is reached.
    #When convergence criteria is satisfied the flag variable is updated to
    #value of 1.0 and the loop condition does not hold
    while (flag == 0.0):
        count += 1
        for i in range(n):
            for j in range(n):
                if(i != j):
                    sum += (a[i][j] * x2[j])
            x2[i] = (b[i] - sum) / a[i][i]
            sum =0.0
        true = [1., -3., 4]
        for i in range(n):
            e[i] = m.fabs(x2[i] - x1[i])
        if(np.all(e < 0.000001)):
            flag = 1.0
        x1 = x2.copy()
    
    #Return the value of the solution vector
    return x2


if __name__ == '__main__':
    #Load the A and B matrices from files
    a = np.loadtxt("coefficient_matrix_1.txt")
    b = np.loadtxt("b_matrix_1.txt")
    
    #Find dimension of A
    n = len(a)
    
    #Open the output file for writing and appending
    f = open("Homework 3 Output.txt", 'w+')
    f.write("HW 3\n==========\n")
    f.write("\nSolution 1:\n")
    f.write("\nThe Coefficient matrix of the given system of equations is as follows:\n")
    for i in range(n):
        for j in range(n):
            f.write(repr(a[i][j]) + "   ")
        f.write("\n")
    f.write("The B matrix is:\n[")
    for i in range(n):
        f.write(repr(b[i]) + " ")
    if(i != n-1):
        f.write(", ")
    f.write("]\n")
    f.write("\n The solution vector for this system of equations is as follows :\n")
    
    #Find the solution
    x = gauss_seidel(a, b, n)
    
    #Write the solution onto the output
    f.write("\n[")
    for i in range(n):
        f.write(repr(x[i]))
        if(i != n-1):
            f.write(",  ")
    f.write("]\n")
    
    #close the file
    f.close()