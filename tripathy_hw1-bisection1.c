#include<stdio.h>
#include<math.h> 

void main()
 {

   int n=1,nmax=100;
   double an=-3.0,bn=-2.0,tol=0.00005,th_err_b=1.0,pn,root1,root2,f_an,f_bn,f_pn;
   printf("For the interval %lf to %lf :: \n",an,bn);
   printf(" The values of an and bn and pn after each interval : \n");
   while(n<=nmax && th_err_b>=tol)
    {
      
      th_err_b=(bn-an)/pow(2.0,n);
      pn=(bn+an)/2;
      
      printf("%d   %lf   %lf    %lf \n",n,an,bn,pn);
      
      f_an=(pow(an,3.0))+(2*pow(an,2))-(3*an)-1;
      f_bn=(pow(bn,3.0))+(2*pow(bn,2))-(3*bn)-1;
      f_pn=(pow(pn,3.0))+(2*pow(pn,2))-(3*pn)-1;
    
      if(f_an<0)
        if (f_pn<0)
            an=pn;
        else
            bn=pn;
      else
        if(f_pn>0)
            an=pn;
       else
            bn=pn;
      n++;   
   }
   
   root1=pn;
   n=1;
   printf("\nThe function has a zero at %lf \n",root1);
   an=-1.0,bn=0.0,tol=0.00005,th_err_b=1.0,pn,root1,root2,f_an,f_bn,f_pn;
   printf("For the interval %lf to %lf :: \n",an,bn);
   printf(" The values of an and bn and pn after each interval : \n");
   while(n<=nmax && th_err_b>=tol)
    {
      
      th_err_b=(bn-an)/pow(2.0,n);
      pn=(bn+an)/2;
      
      printf("%d   %lf   %lf    %lf \n",n,an,bn,pn);
      
      f_an=(pow(an,3.0))+(2*pow(an,2))-(3*an)-1;
      f_bn=(pow(bn,3.0))+(2*pow(bn,2))-(3*bn)-1;
      f_pn=(pow(pn,3.0))+(2*pow(pn,2))-(3*pn)-1;
    
      if(f_an<0)
        if (f_pn<0)
            an=pn;
        else
            bn=pn;
      else
        if(f_pn>0)
            an=pn;
       else
            bn=pn;
      n++;   
   }
   
   root2=pn;
   printf("\nThe function has a zero at %lf \n",root2);
   

}
