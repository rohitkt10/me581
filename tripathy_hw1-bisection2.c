#include<stdio.h>
#include<math.h> 

void main()
 {

   int n=1,nmax=100,i;
   double an=1.0,bn=2.0,tol=0.00005,th_err_b=1.0,pn,root,f_an,f_bn,f_pn,power,pi=3.14159;
   printf(" The values of an and bn and pn after each interval : \n");
   while(n<=nmax && th_err_b>=tol)
    {
      power=1.0;
      for(i=1;i<=n;i++)
        power=power*2;
      th_err_b=(bn-an)/power;
      pn=(bn+an)/2;
      
      printf("%d   %lf   %lf    %lf \n",n,an,bn,pn);
      
      f_an=tan(pi*an)-an-6;
      f_bn=tan(pi*bn)-bn-6;
      f_pn=tan(pi*pn)-pn-6;
    
      if(f_an<0)
        if (f_pn<0)
            an=pn;
        else
            bn=pn;
      else
        if(f_pn>0)
            an=pn;
       else
            bn=pn;
      n++;   
   }
   
   root=pn;

   printf("\nThe next smallest positive root is %lf \n",root);

}
