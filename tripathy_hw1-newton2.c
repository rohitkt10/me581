#include<stdio.h>
#include<math.h> 

double f_x(double x)
   {
       double y;
       y=(pow(x,3.0))-(pow(x,2.0))-(10.0*x)+7;
       return y;
   }

double f_1(double x)
   {
       double y;
       y=(3*pow(x,2.0))-(2.0*x)-10;
       return y;
   }

void main()
  {
     int n=1,nmax=100; 
     double p1,p2,tol=0.000001,init=1.0,error=1.0,func,derv;
     printf("From graphical examination of the given function it is established that it has 3 real roots in the intervals (-4,-3), (0,1) and(3,4). \n");
     printf("For the interval (0,1) : \n");
     printf("The initial value for the given function is chosen as %lf \n",init);
     printf("The value of the approximation of the root and the difference between successive iterations after each iteration : \n");
     p1=init;
     printf("Root            p(n)-p(n-1) \n");
     while(n<=nmax && error>=tol) 
       {
           func=f_x(p1);
           derv=f_1(p1);
           p2=p1-(func/derv);
           error=fabs(p2-p1);
           printf("%lf       %lf \n",p2,error);
           n++;
           p1=p2;
       }
     n--;
     printf(" The solution converged after %d iterations and the approximate root obtained is %lf \n",n,p2);
     init=-3.0,error=1.00;
     n=1;
     printf("For the interval (-4,-3) : \n");
     printf("The initial value for the given function is chosen as %lf \n",init);
     printf("The value of the approximation of the root and the difference between successive iterations after each iteration : \n");
     p1=init;
     printf("Root            p(n)-p(n-1) \n");
     while(n<=nmax && error>=tol) 
       {
           func=f_x(p1);
           derv=f_1(p1);
           p2=p1-(func/derv);
           error=fabs(p2-p1);
           printf("%lf       %lf \n",p2,error);
           n++;
           p1=p2;
       }
     n--;
     printf(" The solution converged after %d iterations and the approximate root obtained is %lf \n",n,p2);
     init=3.0,error=1.00;
     n=1;
     printf("For the interval (3,4) : \n");
     printf("The initial value for the given function is chosen as %lf \n",init);
     printf("The value of the approximation of the root and the difference between successive iterations after each iteration : \n");
     p1=init;
     printf("Root            p(n)-p(n-1) \n");
     while(n<=nmax && error>=tol) 
       {
           func=f_x(p1);
           derv=f_1(p1);
           p2=p1-(func/derv);
           error=fabs(p2-p1);
           printf("%lf       %lf \n",p2,error);
           n++;
           p1=p2;
       }
     n--;
     printf(" The solution converged after %d iterations and the approximate root obtained is %lf \n",n,p2);
   
}
