#include<stdio.h>
#include<math.h> 

double f_x(double x)
   {
       double y;
       y=1.05-(1.04*x)+(log(x));
       return y;
   }

double f_1(double x)
   {
       double y;
       y=-1.04+(1.0/x);
       return y;
   }

void main()
  {
     int n=1,nmax=100; 
     double p1,p2,tol=0.000001,init=0.50,error=1.0,func,derv;
     printf("From graphical examination of the given function it is seen that it has 2 real roots-one in the interval (0,1) and another in the interval (1,2) \n");
     printf("For the interval (0,1) : \n");
     printf("The initial value for the given function is chosen as %lf \n",init);
     printf("The value of the approximation of the root and the difference in the approximations between successive iterations : \n");
     p1=init;
     printf("Root            p(n)-p(n-1) \n");
     while(n<=nmax && error>=tol) 
       {
           func=f_x(p1);
           derv=f_1(p1);
           p2=p1-(func/derv);
           error=fabs(p2-p1);
           printf("%lf       %lf \n",p2,error);
           n++;
           p1=p2;
       }
     n--;
     printf(" The solution converged after %d iterations and the approximate root obtained is %lf \n",n,p2);
     n=1;
     init=1.00,error=1.00;  
     printf("For the interval (1,2) : \n");
     printf("The initial value for the given function is chosen as %lf \n",init);
     printf("The value of the approximation of the root and the difference in the approximations between successive iterations : \n");
     p1=init;
     printf("Root            p(n)-p(n-1) \n");
     while(n<=nmax && error>=tol) 
       {
           func=f_x(p1);
           derv=f_1(p1);
           p2=p1-(func/derv);
           error=fabs(p2-p1);
           printf("%lf       %lf \n",p2,error);
           n++;
           p1=p2;
       }
     n--;
     printf(" The solution converged after %d iterations and the approximate root obtained is %lf \n",n,p2);
   }
